package com.bencoding.connectors;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Properties;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;

import com.bencoding.swift.RunLoginAndTest;
import com.bencoding.utils.NMTLogger;

public class HTTPSConnectByPassCerts {
	
	
	public static void main(String[] args) throws IOException {
		//Some custom method to craete HTTP post object
//		HttpPost post = createPostRequest(); 
		Properties properties;
		(properties = new Properties()).load(RunLoginAndTest.class.getClassLoader().getResourceAsStream("config.properties"));
		
		String urlAdress = properties.getProperty("URL");
		String contentType = properties.getProperty("contentType");
		String inMsg = properties.getProperty("loginMessage");
		String requestMessage = properties.getProperty("requestMessage");
		
		HttpPost post = new HttpPost(urlAdress);
		post.addHeader("Content-Type", contentType);
		
		InputStream inputStream=new ByteArrayInputStream(inMsg.getBytes());//init your own inputstream
		InputStreamEntity inputStreamEntity=new InputStreamEntity(inputStream);
		post.setEntity(inputStreamEntity);

		 
		//Get http client
		CloseableHttpClient httpClient = getCloseableHttpClient();
		 
		//Execute HTTP method
		CloseableHttpResponse res = httpClient.execute(post);
		 
		//Verify response
		if(res.getStatusLine().getStatusCode() == 200)
		{
		    String json = EntityUtils.toString(res.getEntity());
		    System.out.println(json);
		}
	}
	
	
	public static CloseableHttpClient getCloseableHttpClient()
	{
	    CloseableHttpClient httpClient = null;
	    try {
	        httpClient = HttpClients.custom().
	                setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE).
	                setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy()
	                {
	                    public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException
	                    {
	                        return true;
	                    }
	                }).build()).build();
	    } catch (KeyManagementException e) {
	    	NMTLogger.loguear(e.toString());
	    } catch (NoSuchAlgorithmException e) {
	    	NMTLogger.loguear(e.toString());
	    } catch (KeyStoreException e) {
	    	NMTLogger.loguear(e.toString());
	    }
	    return httpClient;
	}
}
