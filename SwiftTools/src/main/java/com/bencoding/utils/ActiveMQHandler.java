package com.bencoding.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.broker.TransportConnector;
import org.apache.activemq.broker.region.Subscription;
//import org.apache.activemq.broker.region.Queue;
import org.apache.activemq.command.ActiveMQQueue;
import org.apache.activemq.command.ActiveMQTopic;

import com.bencoding.config.Variables;

public class ActiveMQHandler {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub
		
		String payload = "test message";
		
		//ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://172.31.31.171:61616");
//		getQueueList("");
//		getQueuesInformation("");
//		getQueueMessages();
	}

	public static String[] getQueueList(String broker) throws JMSException, URISyntaxException {
	    ActiveMQConnection conn = getActiveMQBrokerConnection(broker);
	    conn.start();
	    Set<ActiveMQQueue> allque= conn.getDestinationSource().getQueues();
	    Iterator<ActiveMQQueue> itr= allque.iterator();
	    String[] queues = new String[allque.size()];
	    Integer i = 0;
	    while(itr.hasNext()){
	      ActiveMQQueue q= itr.next();
	      queues[i] = q.getQueueName();
	      i++;
//	      System.out.println(q.getQueueName());
	    }
	    conn.close();
	    Arrays.sort(queues);
	    return queues;
	}

	public static ActiveMQConnectionFactory connectoToBroker(String broker) {
		// TODO Auto-generated method stub
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(Variables.DirWork + "qmgrs.properties"));
			String hostname = prop.getProperty(broker + ".hostname");
			String port = prop.getProperty(broker + ".port");
			return new ActiveMQConnectionFactory("tcp://" + hostname + ":" + port);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	public static ActiveMQConnection getActiveMQBrokerConnection(String broker) throws JMSException, URISyntaxException {
		// TODO Auto-generated method stub
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(Variables.DirWork + "qmgrs.properties"));
			String hostname = prop.getProperty(broker + ".hostname");
			String port = prop.getProperty(broker + ".port");
			ActiveMQConnection conn = ActiveMQConnection.makeConnection("tcp://" + hostname + ":" + port);
			return conn;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
				
		
	}
	
	
	public static int getQueueSize(Session session, Queue queue) {
	    int count = 0;
	    try {
	        QueueBrowser browser = session.createBrowser(queue);
	        Enumeration elems = browser.getEnumeration();
	        while (elems.hasMoreElements()) {
	            elems.nextElement();
	            count++;
	        }
	    } catch (JMSException ex) {
	        ex.printStackTrace();
	    }
	    return count;
	}
	
	public static int getQueueMessages(Session session, Queue queue) {
	    int count = 0;
	    try {
	        QueueBrowser browser = session.createBrowser(queue);
	        Enumeration elems = browser.getEnumeration();
	        while (elems.hasMoreElements()) {
	        	
	        	Message message = (Message) elems.nextElement();
	        	if (message instanceof TextMessage) {
	 		        TextMessage textMessage = (TextMessage) message;
	 		        System.out.println(((TextMessage) message).getText());
//	 		        System.out.println("Incomming Message: '" + textMessage.getText() + "'");
	 		    }
	            elems.nextElement();
	        }
	    } catch (JMSException ex) {
	        ex.printStackTrace();
	    }
	    return count;
	}
	
	public static Session getActiveMQSession(ActiveMQConnectionFactory activeMQConnectionFactory, String broker) throws JMSException {
		
		Connection connection = activeMQConnectionFactory.createConnection("admin", "admin");
		connection.start();
		Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		return session;
		
	}

}
