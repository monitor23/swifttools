package com.bencoding.utils;
/**
 * @author ezencinas
 * 
 * Aplicacion para hacer deploys de objetos broker
 * Para cada operacion que recibe hace checkout del svn del proyecto yo todas sus dependencias y 
 * arma el .bar
 *
 */
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.bencoding.config.Variables;


public class NMTLogger {
	public static final Logger logger = Logger.getLogger("general");
	public static final String logsPath = Variables.DirWork + "\\Logs\\";
	
	public static void loguear(String s) {
		// TODO Auto-generated method stub
		BasicConfigurator.configure();
		PropertyConfigurator.configure(Variables.DirWork + "\\log4j.properties");
		logger.debug(s);
	}
}
