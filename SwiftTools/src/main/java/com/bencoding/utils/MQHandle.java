package com.bencoding.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Hashtable;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.jms.JMSException;

import com.bencoding.config.Variables;
import com.bencoding.swift.TestMessage;
import com.bencoding.swift.Utils;
import com.ibm.mq.MQException;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.constants.CMQC;
import com.ibm.mq.constants.CMQCFC;
import com.ibm.mq.constants.MQConstants;
import com.ibm.mq.headers.MQDataException;
import com.ibm.mq.headers.pcf.PCFMessage;
import com.ibm.mq.headers.pcf.PCFMessageAgent;
import com.ibm.mq.pcf.MQCFH;
import com.ibm.mq.pcf.MQCFIN;
import com.ibm.mq.pcf.MQCFST;
import com.ibm.mq.pcf.PCFAgent;
import com.ibm.mq.pcf.PCFParameter;

@SuppressWarnings("deprecation")
@ManagedBean(name="mq")
@ViewScoped


public class MQHandle {
	
	public static MQQueueManager connectarQM(String qmgr) throws MQException, IOException {
		Hashtable<String, Object> mqht;
		mqht = new Hashtable<String, Object>();
		
		Properties prop = new Properties();
		prop.load(new FileInputStream(Variables.DirWork + "qmgrs.properties"));
//        (prop = new Properties()).load(TestMessage.class.getClassLoader().getResourceAsStream("config.properties"));
		
		mqht.put(CMQC.CHANNEL_PROPERTY, prop.getProperty(qmgr + ".channel"));
		mqht.put(CMQC.HOST_NAME_PROPERTY, prop.getProperty(qmgr + ".hostname"));
		mqht.put(CMQC.PORT_PROPERTY, Integer.parseInt(prop.getProperty(qmgr + ".port")));
		mqht.put(MQConstants.USER_ID_PROPERTY, prop.getProperty(qmgr + ".user"));
        mqht.put(MQConstants.PASSWORD_PROPERTY, prop.getProperty(qmgr + ".password"));
        
        // In case that the qmgr is using ssl I need to add more properties
        boolean isUsingSSL = Boolean.parseBoolean(prop.getProperty(qmgr + ".ssl"));
        if (isUsingSSL) {
        	 // Disabling IBM cipher suite mapping due to 
            // using Oracle Java and not IBM Java
            System.setProperty("com.ibm.mq.cfg.useIBMCipherMappings", "false");
            // Enabling SSL debug to view the communication
            //System.setProperty("javax.net.debug", "ssl:handshake");

            System.setProperty("javax.net.ssl.trustStore",prop.getProperty("GENERAL.TRUSTORE"));
            System.setProperty("javax.net.ssl.trustStorePassword",prop.getProperty("GENERAL.TRUSTORE_PASSWORD"));
            System.setProperty("javax.net.ssl.keyStore",prop.getProperty("GENERAL.KEYSTORE"));
            System.setProperty("javax.net.ssl.keyStorePassword",prop.getProperty("GENERAL.KEYSTORE_PASSWORD"));

            mqht.put(MQConstants.SSL_CIPHER_SUITE_PROPERTY, prop.getProperty(qmgr + ".ssl_cipher"));
        }
        MQQueueManager qMgr = new MQQueueManager(qmgr, mqht);
        return qMgr;
	}

	public String[] getQmgrs() {
		String qmgrs[] = Utils.obtenerCombosOrdenados("qmgrs.properties", ".name");
		if (qmgrs != null) {
			for (int j = 0; j < qmgrs.length; j++) {
				qmgrs[j].toString();
			}
		}
		return qmgrs;
	}
	
	
	
	public static String[] getQueuesFromQmgr(MQQueueManager qm) throws MQException {
		String[] listQ = null;
		try {
			PCFAgent agent;
			MQMessage[] responses;
			MQCFH cfh;
			PCFParameter p;
			PCFParameter[] parameters = { new MQCFST(CMQC.MQCA_Q_NAME, "*"),new MQCFIN(MQConstants.MQIA_Q_TYPE, MQConstants.MQQT_LOCAL) };
			agent = new PCFAgent(qm);
			responses = agent.send(MQConstants.MQCMD_INQUIRE_Q, parameters);
			int CantColas = 0;
			int cantQ = 0;
			String qname = null;
			listQ = new String[responses.length];
			while (CantColas < responses.length) {
				cfh = new MQCFH(responses[CantColas]);
				if (cfh.reason == 0) {
					for (int i = 0; i < cfh.parameterCount; i++) {
						// Walk through the returned attributes
						p = PCFParameter.nextParameter(responses[CantColas]);
						int parm = p.getParameter();
						if (parm == MQConstants.MQCA_Q_NAME) {
							qname = p.getValue().toString().trim();
							i=cfh.parameterCount;
							if ((!qname.contains("SYSTEM.")) && (!qname.contains("AMQ."))) {	
								listQ[cantQ] = qname;
								cantQ++;
							}
						}
					}
				}
				CantColas++;
			}
		} catch (MQException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
//			qm.disconnect();
		}
		// Remuevo los valores nulos del array para que no tire error
		listQ = Utils.removeNullValuesInArray(listQ);
		return listQ;
	}
	
	public static String[] getQmgrQueuesFromProvider(String qmgr){
		MQQueueManager qm;
		String provider;
		String[] listQueues = null;
		PCFMessageAgent agent = null;
		String[] listQ = null;
		provider = Utils.getMQProvider(qmgr);
		switch (provider.toUpperCase()) {
		case "IBM":
			try {
				qm = MQHandle.connectarQM(qmgr);
				agent = new PCFMessageAgent(qm); // I use it to show queues later 
				listQ = MQHandle.getQueuesFromQmgr(qm);
				listQueues = listQ;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Could not connect to queue manager"));
				NMTLogger.loguear(e.toString());
			} catch (MQDataException e) {
				// TODO Auto-generated catch block
				NMTLogger.loguear(e.toString());
			} catch (MQException e) {
				// TODO Auto-generated catch block
				
				System.out.println(" error: " + e.toString());
				NMTLogger.loguear(e.toString());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", e.getMessage()));
			}
			break;
		case "APACHE":
			try {
				listQueues = ActiveMQHandler.getQueueList(qmgr);
			} catch (JMSException | URISyntaxException e) {
				// TODO Auto-generated catch block
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Could not connect to ActiveMQ Broker"));
				e.printStackTrace();
			}
			break;
		default:
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Provider not found"));
			break;
		}
		return listQueues;
	}
	
}
