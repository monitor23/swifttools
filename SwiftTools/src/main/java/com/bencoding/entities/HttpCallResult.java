package com.bencoding.entities;

public class HttpCallResult {
	
	
	private String responseMessage;
	private String requestMessage;
	private String reasonPhrase;
	private int statusCode;
	private String description;
	private String position;
	private String rowClass;
	
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public String getRequestMessage() {
		return requestMessage;
	}
	public void setRequestMessage(String requestMessage) {
		this.requestMessage = requestMessage;
	}
	public String getReasonPhrase() {
		return reasonPhrase;
	}
	public void setReasonPhrase(String reasonPhrase) {
		this.reasonPhrase = reasonPhrase;
	}
	public int getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPosition() {
		return position;
	}
	public void setPosition(String position) {
		this.position = position;
	}
	public String getRowClass() {
		return rowClass;
	}
	public void setRowClass(String rowClass) {
		this.rowClass = rowClass;
	} 
	
	public HttpCallResult(String responseMessage, String requestMessage, String reasonPhrase,
			int statusCode, String description, String position, String rowClass) {
		super();
		this.responseMessage = responseMessage;
		this.requestMessage = requestMessage;
		this.reasonPhrase = reasonPhrase;
		this.statusCode = statusCode;
		this.description = description;
		this.position = position;
		this.rowClass = rowClass;
	}
	
	public HttpCallResult(){
		
	}
	
	
	

}
