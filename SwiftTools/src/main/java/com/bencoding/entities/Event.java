package com.bencoding.entities;

public class Event {
	
	String version;
    String date;
    String description;
    
    public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Event() {
    }

    public Event(String version, String date, String description) {
        this.version = version;
        this.date = date;
        this.description = description;
    }


}
