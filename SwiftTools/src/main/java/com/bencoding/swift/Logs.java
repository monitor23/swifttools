package com.bencoding.swift;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import com.bencoding.entities.Event;
import com.bencoding.utils.NMTLogger;
import com.bencoding.utils.ReverseLineInputStream;


@ManagedBean(name="logs")
@ViewScoped

public class Logs {
	
	private List<Event> events;
	private String console = "";

    public String getConsole() {
		return console;
	}

	public void setConsole(String console) {
		this.console = console;
	}

	@PostConstruct
    public void init() {
        events = new ArrayList<>();
        // add first the last change
        events.add(new Event("1.0.0", "16/12/2021", "Put Messages: Added option to read message from sample folder"));
        events.add(new Event("1.0.0", "13/09/2021", "Changed file name for LAU generated messages"));
        events.add(new Event("1.0.0", "10/09/2021", "ActiveMQ included to AGI and MQ Browse"));
        events.add(new Event("1.0.0", "28/05/2021", "Added digest calculator"));
        events.add(new Event("1.0.0", "12/04/2021", "Modified MT199 calls to use sample messages"));
        events.add(new Event("1.0.0", "06/04/2021", "Added changelog and log4j"));
        events.add(new Event("1.0.0", "25/03/2021", "Connection to queue managers using SSL"));
        
    }

    public List<Event> getEvents() {
        return events;
    }
    
    public void getLogs() throws EOFException, IOException{
    	
    	File file =  new File(NMTLogger.logsPath + "general.log");
    	BufferedReader  in = new BufferedReader (new InputStreamReader (new ReverseLineInputStream(file)));

    	while(true) {
    	    String line = in.readLine();
    	    if (line == null) {
    	        break;
    	    }
    	    console += line + "\n";
    	}
    	
    	in.close();
	} 
    
    
}
