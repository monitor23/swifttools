package com.bencoding.swift;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;
import org.primefaces.model.file.UploadedFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.bencoding.connectors.HTTPSConnectByPassCerts;
import com.bencoding.entities.HttpCallResult;
import com.bencoding.utils.NMTLogger;


@SuppressWarnings("deprecation")
@ManagedBean(name="soapha")
@ViewScoped

public class RunLoginAndTest {
	
	private final String nsEnvelope = "http://schemas.xmlsoap.org/soap/envelope/";
	private final String nsSwift = "urn:swift:saa:xsd:soapha";
	
	private final char CR  = (char) 0x0D;
	private final char LF  = (char) 0x0A; 
	private final String CRLF  = "" + CR + LF; 
	
	private String endpoint = "https://192.168.11.12:48200/soapha/";
	private String send_bic12 = "PTSQGBB0AAAA";//"PTSQGBB0AAAA";
	private String send_x1 = "";//"PTSQGBB0AAA";
	private String recv_bic12 = "PTSQGBB0XAAA";//"PTSQGBB0XAAA";
	private String recv_x1 = "";//"PTSQGBB0AAA";
	private boolean send = true;
	private boolean get = true;
	private boolean render = false;
	private String msgParam = "";
	private HttpCallResult httpCallResult;
	private List<HttpCallResult> httpCallResults;
	private String field20 = "TRN MSG1000";
	private String field79 = "MESSAGE TEXT";
	private String send_partner = "SOAPSEND";
	private String recv_partner = "SOAPGET";
	private String inputMode = "Form";
	private UploadedFile uploadedFile = null;
	
	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public String getInputMode() {
		return inputMode;
	}

	public void setInputMode(String inputMode) {
		this.inputMode = inputMode;
	}

	public String getSend_partner() {
		return send_partner;
	}

	public void setSend_partner(String send_partner) {
		this.send_partner = send_partner;
	}

	public String getRecv_partner() {
		return recv_partner;
	}

	public void setRecv_partner(String recv_partner) {
		this.recv_partner = recv_partner;
	}

	public String getField20() {
		return field20;
	}

	public void setField20(String field20) {
		this.field20 = field20;
	}

	public String getField79() {
		return field79;
	}

	public void setField79(String field79) {
		this.field79 = field79;
	}

	public String getMsgParam() {
		return msgParam;
	}

	public void setMsgParam(String msgParam) {
		this.msgParam = msgParam;
	}

	public HttpCallResult getHttpCallResult() {
		return httpCallResult;
	}

	public void setHttpCallResult(HttpCallResult httpCallResult) {
		this.httpCallResult = httpCallResult;
	}

	public List<HttpCallResult> getHttpCallResults() {
		return httpCallResults;
	}

	public void setHttpCallResults(List<HttpCallResult> httpCallResults) {
		this.httpCallResults = httpCallResults;
	}

	public boolean isRender() {
		return render;
	}

	public void setRender(boolean render) {
		this.render = render;
	}

	public boolean isSend() {
		return send;
	}

	public void setSend(boolean send) {
		this.send = send;
	}

	public boolean isGet() {
		return get;
	}

	public void setGet(boolean get) {
		this.get = get;
	}
	
	public String getSend_bic12() {
		return send_bic12;
	}

	public void setSend_bic12(String send_bic12) {
		this.send_bic12 = send_bic12;
	}

	public String getSend_x1() {
		return send_x1;
	}

	public void setSend_x1(String send_x1) {
		this.send_x1 = send_x1;
	}

	public String getRecv_bic12() {
		return recv_bic12;
	}

	public void setRecv_bic12(String recv_bic12) {
		this.recv_bic12 = recv_bic12;
	}

	public String getRecv_x1() {
		return recv_x1;
	}

	public void setRecv_x1(String recv_x1) {
		this.recv_x1 = recv_x1;
	}

	public String getEndpoint() {
		return endpoint;
	}

	public void setEndpoint(String endpoint) {
		this.endpoint = endpoint;
	}

	public void run() {
		// TODO Auto-generated method stub
		httpCallResults = new ArrayList<HttpCallResult>();
		Properties properties;
		try {
			// Properties files contains configuration needed to call the web services
			(properties = new Properties()).load(RunLoginAndTest.class.getClassLoader().getResourceAsStream("config.properties"));
			
			String contentType = properties.getProperty("contentType");
			Boolean sendMessage = true;
			if(send == true) {
				// Do login
				httpCallResult = new HttpCallResult();
				httpCallResult.setDescription("Open SOAPSEND");
				String requestMessage = properties.getProperty("loginMessageSend");
				requestMessage = requestMessage.replace("<urn:MessagePartnerName></urn:MessagePartnerName>", "<urn:MessagePartnerName>" + send_partner + "</urn:MessagePartnerName>");
				httpCallResult.setRequestMessage(prettyFormat(requestMessage,2));
				CloseableHttpResponse response = runHTTP(endpoint, contentType, requestMessage, 30, "POST");
				int statusCode = response.getStatusLine().getStatusCode();
				httpCallResult.setStatusCode(statusCode);
				httpCallResult.setReasonPhrase(response.getStatusLine().getReasonPhrase());
				String responseMessage = getMessageFromResponse(response);
				Document doc = Digest.parseAsXml(responseMessage);
				String token = doc.getElementsByTagNameNS(nsSwift,"SessionToken").item(0).getTextContent();
				
//				responseMessage = responseMessage.substring(responseMessage.indexOf("<SOAP-ENV:Envelope"),responseMessage.indexOf("</SOAP-ENV:Envelope>")+20);
//				System.out.println(loginReponseMessage);
				httpCallResult.setResponseMessage(prettyFormat(responseMessage,2));
				httpCallResults.add(httpCallResult);
				render = true; // I put the render here to avoid de "no results" message in the table before i get the first response
				// if login worked fine y send the next message
				if (statusCode == 200) {
					httpCallResult = new HttpCallResult();
					httpCallResult.setDescription("FIN message");
					requestMessage = properties.getProperty("requestMessageSend");
					//String token = responseMessage.substring(responseMessage.indexOf("<ns2:SessionToken>")+18,responseMessage.indexOf("</ns2:SessionToken>"));
					if (!token.equals("")) {
						String base64Value = "";
						if (inputMode.equals("Form")) {
							String field20ToEncode = CRLF + ":20:" + field20; 
							String field79ToEncode = CRLF +  ":79:" + field79;
							String fieldsToEncode = field20ToEncode + field79ToEncode;
							base64Value = Base64.getEncoder().encodeToString(fieldsToEncode.getBytes());
						} else {
							// File mode
							if ((uploadedFile != null) && (uploadedFile.getFileName() != null)) {
								String inMessage = Utils.convertBufferedReaderToString(uploadedFile);
								String block1 = inMessage.substring(inMessage.indexOf("{1:"),inMessage.indexOf("}"));
								String block2 = inMessage.substring(inMessage.indexOf("{2:"));
								block2 = block2.substring(0,block2.indexOf("}"));
								String block4 = inMessage.substring(inMessage.indexOf("4:"));
								block4 = block4.substring(0,block4.indexOf("}"));
								send_bic12 = block1.substring(6,18);
								send_x1 =  send_bic12.substring(0,8) + send_bic12.substring(9,12);
								recv_bic12 = block2.substring(7,19);
								recv_x1 = recv_bic12.substring(0,8) + recv_bic12.substring(9,12);
								
								String field20 = block4.substring(block4.indexOf(":20:")+4);
								field20  = field20.substring(0,field20.indexOf(":"));
								field20 = field20.replace("\n", "");
								String field20ToEncode = CRLF + ":20:" + field20;
								
								
								String field79 = block4.substring(block4.indexOf(":79:")+4);
								field79  = field79.substring(0,field79.indexOf("-"));
								field79 = field79.replace("\n", "");
								String field79ToEncode = CRLF +  ":79:" + field79;
								
								String fieldsToEncode = field20ToEncode + field79ToEncode;
								base64Value = Base64.getEncoder().encodeToString(fieldsToEncode.getBytes());
							} else {
								sendMessage = false;
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning:", "Please select a file"));
							}
						}
						if (sendMessage) {
							requestMessage = requestMessage.replace("<urn:SessionToken></urn:SessionToken>", "<urn:SessionToken>" + token + "</urn:SessionToken>");
							requestMessage = requestMessage.replace("<Sender><BIC12></BIC12><FullName><X1></X1></FullName></Sender>", "<Sender><BIC12>" + send_bic12 + "</BIC12><FullName><X1>" + send_bic12.substring(0,8) + send_bic12.substring(9,12) + "</X1></FullName></Sender>");
							requestMessage = requestMessage.replace("<Receiver><BIC12></BIC12><FullName><X1></X1></FullName></Receiver>", "<Receiver><BIC12>" + recv_bic12 + "</BIC12><FullName><X1>" + recv_bic12.substring(0,8) + recv_bic12.substring(9,12) + "</X1></FullName></Receiver>");
							requestMessage = requestMessage.replace("<Body></Body>", "<Body>" + base64Value + "</Body>");
							
							httpCallResult.setRequestMessage(prettyFormat(requestMessage,2));
							response = runHTTP(endpoint, contentType, requestMessage, 30, "POST");
							statusCode = response.getStatusLine().getStatusCode();
							httpCallResult.setStatusCode(statusCode);
							httpCallResult.setReasonPhrase(response.getStatusLine().getReasonPhrase());
							responseMessage = getMessageFromResponse(response);
//							responseMessage = responseMessage.substring(responseMessage.indexOf("<SOAP-ENV:Envelope"),responseMessage.indexOf("</SOAP-ENV:Envelope>")+20);
							httpCallResult.setResponseMessage(prettyFormat(responseMessage,2));
							httpCallResults.add(httpCallResult);
						}
					}
				}
			}
			if (sendMessage) {
				if(get == true) {
					httpCallResult = new HttpCallResult();
					httpCallResult.setDescription("Open SOAPGET");
					String requestMessage = properties.getProperty("loginMessageGet");
					requestMessage = requestMessage.replace("<urn:MessagePartnerName></urn:MessagePartnerName>", "<urn:MessagePartnerName>" + recv_partner + "</urn:MessagePartnerName>");
					httpCallResult.setRequestMessage(prettyFormat(requestMessage,2));
					
					CloseableHttpResponse response = runHTTP(endpoint, contentType, requestMessage, 30, "POST");
					int statusCode = response.getStatusLine().getStatusCode();
					httpCallResult.setStatusCode(statusCode);
					httpCallResult.setReasonPhrase(response.getStatusLine().getReasonPhrase());
					String responseMessage = getMessageFromResponse(response);
//					responseMessage = responseMessage.substring(responseMessage.indexOf("<SOAP-ENV:Envelope"),responseMessage.indexOf("</SOAP-ENV:Envelope>")+20);
					httpCallResult.setResponseMessage(prettyFormat(responseMessage,2));
					httpCallResults.add(httpCallResult);
					render = true;
					// if login worked fine y send the next message
					if (statusCode == 200) {
						httpCallResult = new HttpCallResult();
						httpCallResult.setDescription("Ack FIN");
						requestMessage = properties.getProperty("requestMessageGet");
//						String token = responseMessage.substring(responseMessage.indexOf("<ns2:SessionToken>")+18,responseMessage.indexOf("</ns2:SessionToken>"));
						Document doc = Digest.parseAsXml(responseMessage);
						String token = doc.getElementsByTagNameNS(nsSwift,"SessionToken").item(0).getTextContent();
						if (!token.equals("")) {
							requestMessage = requestMessage.replace("<urn:SessionToken></urn:SessionToken>", "<urn:SessionToken>" + token + "</urn:SessionToken>");
							httpCallResult.setRequestMessage(prettyFormat(requestMessage,2));
							response = runHTTP(endpoint, contentType, requestMessage, 30, "POST");
							statusCode = response.getStatusLine().getStatusCode();
							httpCallResult.setStatusCode(statusCode);
							httpCallResult.setReasonPhrase(response.getStatusLine().getReasonPhrase());
							responseMessage = getMessageFromResponse(response);
//							responseMessage = responseMessage.substring(responseMessage.indexOf("<SOAP-ENV:Envelope"),responseMessage.indexOf("</SOAP-ENV:Envelope>")+20);
							httpCallResult.setResponseMessage(prettyFormat(responseMessage,2));
							httpCallResults.add(httpCallResult);
						}
					}
				}
			}
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", e.toString()));
			NMTLogger.loguear(e.toString());
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: Response message could not be paresed as XML", e.toString()));
			e.printStackTrace();
		}
	}
	
	
	
	public static CloseableHttpResponse runHTTP(String urlAdress, String contentType, String inMsg, int timeout, String method) throws ClientProtocolException, IOException {
		
		HttpParams httpParams = new BasicHttpParams();
	    HttpConnectionParams.setConnectionTimeout(httpParams, timeout * 1000);
	    CloseableHttpClient httpClient = HTTPSConnectByPassCerts.getCloseableHttpClient();
		HttpPost httppost = new HttpPost(urlAdress);
		HttpGet httpget = new HttpGet(urlAdress);
		CloseableHttpResponse response = null;
		
		switch(method.toUpperCase()) {
			case "POST":
				httppost.addHeader("Content-Type", contentType);
				InputStream inputStream=new ByteArrayInputStream(inMsg.getBytes());//init your own inputstream
				InputStreamEntity inputStreamEntity=new InputStreamEntity(inputStream);
				httppost.setEntity(inputStreamEntity);
				response = httpClient.execute(httppost);
		    // code block
		    break;
			case "GET":
		    // code block
				httpget.addHeader("Content-Type", contentType);
				response = httpClient.execute(httpget);
		    break;
		}
        // Get HttpResponse Status
		/*
        System.out.println(response.getProtocolVersion());              // HTTP/1.1
        System.out.println(response.getStatusLine().getStatusCode());   // 200
        System.out.println(response.getStatusLine().getReasonPhrase()); // OK
        System.out.println(response.getStatusLine().toString());        // HTTP/1.1 200 OK
		*/
		
		return response;
	}
	
	public static String getMessageFromResponse(CloseableHttpResponse response) {
		String outMsg = "";
		HttpEntity entity = response.getEntity();
		if (entity != null) {
          // return it as a String
			try {
				outMsg = EntityUtils.toString(entity);
			} catch (ParseException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return outMsg;
	}
	
	public static String prettyFormat(String input, int indent) {
		try {
			Source inMessage = new StreamSource(new StringReader(input));
			StringWriter stringWriter = new StringWriter();
			StreamResult xmlOutput = new StreamResult(stringWriter);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute("indent-number", indent);
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty("omit-xml-declaration","yes");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(inMessage, xmlOutput);
			return xmlOutput.getWriter().toString();
		} catch (Exception e) {
			return input;
//				throw new RuntimeException(e); // simple exception handling, please
											// review it
		}
	}
		
}
