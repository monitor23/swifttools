package com.bencoding.swift;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jdom2.JDOMException;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.file.UploadedFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import com.bencoding.utils.NMTLogger;

@ManagedBean(name="digest")
@ViewScoped

public class Digest {
	
	private boolean saveToFileEnable = false;
	private UploadedFile uploadedXML = null;
	private UploadedFile uploadedOrigin = null;
	private String newMessage = "";
	private byte[] finalMessage;
	private StreamedContent file;
	
	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public String getNewMessage() {
		return newMessage;
	}

	public void setNewMessage(String newMessage) {
		this.newMessage = newMessage;
	}

	public boolean isSaveToFileEnable() {
		return saveToFileEnable;
	}

	public void setSaveToFileEnable(boolean saveToFileEnable) {
		this.saveToFileEnable = saveToFileEnable;
	}

	public UploadedFile getUploadedXML() {
		return uploadedXML;
	}

	public void setUploadedXML(UploadedFile uploadedXML) {
		this.uploadedXML = uploadedXML;
	}

	public UploadedFile getUploadedOrigin() {
		return uploadedOrigin;
	}

	public void setUploadedOrigin(UploadedFile uploadedOrigin) {
		this.uploadedOrigin = uploadedOrigin;
	}

	public void addDigestToXml() {
		
		String inXML = new String();
		BufferedInputStream bis = null;
		if ((uploadedOrigin.getFileName() != null) && (uploadedXML.getFileName() != null)) {
			try {
				InputStream stream = uploadedOrigin.getInputStream();
	            bis = new BufferedInputStream(stream);
	            byte[] DigestValueBinary = {0};
	            DigestValueBinary = getDigestSHA256(bis);
	            String encoded = Base64.getEncoder().encodeToString(DigestValueBinary);
	        	
	        	inXML = Utils.convertBufferedReaderToStringWithCR(uploadedXML);
                Document doc = parseAsXml(inXML);
                doc.getDocumentElement().normalize();
                Node root = doc.getElementsByTagName("Saa:DataPDU").item(0);
                Node body = doc.getElementsByTagName("Saa:Body").item(0);
                Node header = doc.getElementsByTagName("Saa:Header").item(0);
                if (header == null) {
                	header.setTextContent("Saa:Header");
                }
                Element message = (Element) doc.getElementsByTagName("Saa:Message").item(0);
                if (message == null) {
                	message = doc.createElement("Saa:Message");
                }
                Element securityInfo = (Element) doc.getElementsByTagName("Saa:SecurityInfo").item(0);
                if (securityInfo == null) {
                	securityInfo = doc.createElement("Saa:SecurityInfo");
                }
                Element SWIFTNetSecurityInfo = (Element) doc.getElementsByTagName("Saa:SWIFTNetSecurityInfo").item(0);
                if (SWIFTNetSecurityInfo == null) {
                	SWIFTNetSecurityInfo = doc.createElement("Saa:SWIFTNetSecurityInfo");
                }
                Element fileDigestAlgorithm = doc.createElement("Saa:FileDigestAlgorithm");
                fileDigestAlgorithm.setTextContent("SHA-256");
                Element fileDigestValue = doc.createElement("Saa:FileDigestValue");
                fileDigestValue.setTextContent(encoded);
                
                SWIFTNetSecurityInfo.appendChild(fileDigestAlgorithm);
                SWIFTNetSecurityInfo.appendChild(fileDigestValue);
                
                Element fileLogicalName = (Element) message.getElementsByTagName("Saa:FileLogicalName").item(0);
                securityInfo.appendChild(SWIFTNetSecurityInfo);
                message.appendChild(securityInfo);
                message.appendChild(fileLogicalName);
                header.appendChild(message);
                root.appendChild(header);
                root.appendChild(body);
                
                newMessage = DocumentToString(doc);
//                System.out.println(output);
                saveToFileEnable = true;
        		newMessage = Utils.prettyFormat(newMessage);
		        finalMessage = newMessage.getBytes(); // I use finalMessage in the download view
		        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", "Digest successfully generated!"));
		        FileDownloadView("digest.xml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				NMTLogger.loguear(e.toString());
			} catch (ParserConfigurationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SAXException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (TransformerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JDOMException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        
		} else {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning:", "Please select a file to upload"));
        }
		
	}
	
	public static Document parseXmlFile(String in) {
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource(new StringReader(in));
			return db.parse(is);
		} catch (ParserConfigurationException e) {
			throw new RuntimeException(e);
		} catch (SAXException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public static Document parseAsXml (String inMessage) throws ParserConfigurationException, SAXException, IOException{
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
		docBuilderFactory.setNamespaceAware(true);
		DocumentBuilder docBuilder;
		docBuilder = docBuilderFactory.newDocumentBuilder();
		Document doc = docBuilder.parse(new InputSource(new java.io.StringReader(inMessage)));
		doc.getDocumentElement().normalize();
		return doc;
	}
	
	public static String DocumentToString(Document doc) throws TransformerException {
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
		StringWriter writer = new StringWriter();
		transformer.transform(new DOMSource(doc), new StreamResult(writer));
		String output = writer.getBuffer().toString().replaceAll("\n|\r", "");
//		System.out.println(output);
		return output;
	}
	
	public void FileDownloadView(String name) {
		InputStream stream = new ByteArrayInputStream( finalMessage );
        file = DefaultStreamedContent.builder()
                .name(name)
                .contentType("text/plain")
                .stream(() -> stream)
                .build();
    }
	
	public static byte[] getDigestSHA256(BufferedInputStream stream) throws Exception {

        // Get SHA-256 digester
        MessageDigest msgDigest = MessageDigest.getInstance("SHA-256");
        //Get the payload in the digester
        int bytesRead = -1;
        byte[] buffer = new byte[10000];
        while ((bytesRead = stream.read(buffer)) > 0) msgDigest.update(buffer, 0, bytesRead);   
        //Get the digest value
        return(msgDigest.digest());

    }
}
