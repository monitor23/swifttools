package com.bencoding.swift;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.servlet.http.HttpSession;

import org.apache.activemq.ActiveMQConnectionFactory;

import com.bencoding.entities.Message;
import com.bencoding.entities.Queue;
import com.bencoding.utils.ActiveMQHandler;
import com.bencoding.utils.MQHandle;
import com.bencoding.utils.NMTLogger;
import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.constants.CMQC;
import com.ibm.mq.constants.CMQCFC;
import com.ibm.mq.headers.MQDataException;
import com.ibm.mq.headers.pcf.PCFMessage;
import com.ibm.mq.headers.pcf.PCFMessageAgent;
import com.ibm.mq.pcf.MQCFH;

@SuppressWarnings("deprecation")
@ManagedBean(name="browseq")
@ViewScoped


public class BrowseQueue {
	
	private String qmgr = "";
	private String queue_name = "";
	private String[] listQueues = null;
	private boolean render = false;
	private boolean renderActiveMQ = false;
	private boolean renderMsgs = false;
	private Queue queue;
	private List<Queue> queues;
	private Message message;
	private List<Message> messages;
	private String queueParam;
	private String  msgParam;
	private String provider;
	
	
	public boolean isRenderActiveMQ() {
		return renderActiveMQ;
	}

	public void setRenderActiveMQ(boolean renderActiveMQ) {
		this.renderActiveMQ = renderActiveMQ;
	}

	MQQueueManager qm;
	PCFMessageAgent agent = null;

	public String getMsgParam() {
		return msgParam;
	}

	public void setMsgParam(String msgParam) {
		this.msgParam = msgParam;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}
	
	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	public List<Message> getMessages() {
		return messages;
	}

	public boolean isRenderMsgs() {
		return renderMsgs;
	}

	public void setRenderMsgs(boolean renderMsgs) {
		this.renderMsgs = renderMsgs;
	}

	public String getQueueParam() {
		return queueParam;
	}

	public void setQueueParam(String queueParam) {
		this.queueParam = queueParam;
	}
	
	public Queue getQueue() {
		return queue;
	}

	public void setQueue(Queue queue) {
		this.queue = queue;
	}

	public List<Queue> getQueues() {
		return queues;
	}

	public void setQueues(List<Queue> queues) {
		this.queues = queues;
	}

	public boolean isRender() {
		return render;
	}

	public void setRender(boolean render) {
		this.render = render;
	}

	public String getQueue_name() {
		return queue_name;
	}

	public void setQueue_name(String queue_name) {
		this.queue_name = queue_name;
	}

	public String getQmgr() {
		return qmgr;
	}

	public void setQmgr(String qmgr) {
		this.qmgr = qmgr;
	}

	public String[] getListQueues() {
		return listQueues;
	}

	public void setListQueues(String[] listQueues) {
		this.listQueues = listQueues;
	}

	public void getQmgrQueues(AjaxBehaviorEvent event){
		
		listQueues = MQHandle.getQmgrQueuesFromProvider(qmgr);
	}
	
	public void showQueues() {
		
		renderMsgs = false;
		provider = Utils.getMQProvider(qmgr);
		switch (provider.toUpperCase()) {
		case "IBM":
			render = true;
//			if (queue_name.equals("*")) {
				try {
					getQueuesFromQmgrFull();
				} catch (MQException e) {
					// TODO Auto-generated catch block
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Could not connect to queue manager"));
					NMTLogger.loguear(e.toString());
				} 
			break;
		case "APACHE":
			renderActiveMQ = true;
			try {
				getQueuesInfoFromActiveMQ();
			} catch (JMSException | URISyntaxException e) {
				// TODO Auto-generated catch block
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Could not connect to ActiveMQ Broker"));
				e.printStackTrace();
			}
			break;
		default:
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Provider not found"));
			break;
		}

	}
	
	public void getMessagesFromQueue() {
		provider = Utils.getMQProvider(qmgr);
		switch (provider.toUpperCase()) {
		case "IBM":
			getMessagesFromQueueForIBM();
			break;
		case "APACHE":
			getMessagesFromQueueForApache();
			break;
		default:
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Provider not found"));
			break;
		}
		
	}
	
	public void getMessagesFromQueueForApache() {
		
		renderActiveMQ = true;
		renderMsgs = true;

		message = new Message();
		messages = new ArrayList<Message>();
		try {
			ActiveMQConnectionFactory activeMQConnectionFactory = ActiveMQHandler.connectoToBroker(qmgr);
			Session session = ActiveMQHandler.getActiveMQSession(activeMQConnectionFactory, qmgr);

			Destination destination = session.createQueue(queue_name);
			queue.name = queue_name;
			// Cargo un m�ximo de 50 mensajes para que no se quede colgado si tiene muchos
			QueueBrowser browser = session.createBrowser((javax.jms.Queue) destination);
	        Enumeration elems = browser.getEnumeration();
	        int i = 0;
	        while (elems.hasMoreElements() && i<50) {
	        	i++;
	        	javax.jms.Message jmsmessage = (javax.jms.Message) elems.nextElement();
	        	message = new Message();
				// Browse the message
	        	Long fecha = jmsmessage.getJMSTimestamp();
				String messageId = jmsmessage.getJMSMessageID();
				String correlId = jmsmessage.getJMSCorrelationID();
				message.setCola(queueParam);
				message.setQmgr(qmgr);
				message.setFecha(fecha.toString());
				message.setMsgid(messageId);
				message.setCorrelid(correlId);
				message.setNumero(i);
				// print the text
				if (jmsmessage instanceof TextMessage) {
	 		        TextMessage textMessage = (TextMessage) jmsmessage;
	 		        String text = textMessage.getText();
	 		        System.out.println(((TextMessage) jmsmessage).getText());
//			 		        System.out.println("Incomming Message: '" + textMessage.getText() + "'");
	 		       message.setMsg(text);
				} else {
					message.setMsg(jmsmessage.toString());
				}
				if (!message.getMsg().equals("")) {
					message.setMsgFormateado(Utils.prettyFormat(message.getMsg(), 2));
				} else {
					message.setMsgFormateado("Message format not recognize");
				}
				
				// move cursor to the next message
				messages.add(message);
//	            elems.nextElement();
	        }
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			
		}
		
	}
	
	public void getMessagesFromQueueForIBM() {
		
		render = true;
		renderMsgs = true;

		message = new Message();
		messages = new ArrayList<Message>();
		boolean thereAreMessages = true;
		MQQueue mq_queue = null;
		try {
			int openOptions = MQC.MQOO_FAIL_IF_QUIESCING | MQC.MQOO_INPUT_SHARED | MQC.MQOO_BROWSE;
			mq_queue = qm.accessQueue(queueParam, openOptions);
			MQMessage theMessage = new MQMessage();
			MQGetMessageOptions gmo = new MQGetMessageOptions();
			gmo.options = MQC.MQGMO_WAIT | MQC.MQGMO_BROWSE_FIRST;
			gmo.matchOptions = MQC.MQMO_NONE;
			gmo.waitInterval = 5000;
			int numMsg = 0;
			// Cargo un m�ximo de 50 mensajes para que no se quede colgado si tiene muchos
			while (thereAreMessages && numMsg < 50) {
				message = new Message();
				numMsg++;
				// Browse the message
				mq_queue.get(theMessage, gmo);
				String fecha = Utils.formatearGregorianCandelar(theMessage.putDateTime);
				String messageId = Utils.byteArrayToHexString(theMessage.messageId);
				String correlId = Utils.byteArrayToHexString(theMessage.correlationId);
				message.setCola(queueParam);
				message.setQmgr(qmgr);
				message.setFecha(fecha);
				message.setMsgid(messageId);
				message.setCorrelid(correlId);
				message.setNumero(numMsg);
				// print the text
				String msg = "";
				String msgFormateado = "";
				try {
//		        	msg = theMessage.readString(theMessage.getMessageLength()).replaceAll("<", "&lt;").replaceAll(">", "&gt;").replaceAll("\"","&quot;");
					msg = theMessage.readString(theMessage.getMessageLength());
				} catch (IOException e) {
					System.out.println(e);

				}
				message.setMsg(msg);
				if (!msg.equals("")) {
					message.setMsgFormateado(Utils.prettyFormat(msg, 2));
				} else {
					message.setMsgFormateado("");
				}
				
				// move cursor to the next message
				gmo.options = MQC.MQGMO_WAIT | MQC.MQGMO_BROWSE_NEXT;
				messages.add(message);
			}

		} catch (MQException e) {
			thereAreMessages = false;
		} finally {
			try {
				mq_queue.close();
			} catch (MQException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	public void clearQueue() {
		
		boolean loopAgain = true;
		MQEnvironment.disableTracing();
		MQException.log = null;

		MQQueue mq_queue = null;
		int openOptions = MQC.MQOO_INQUIRE + MQC.MQOO_FAIL_IF_QUIESCING + MQC.MQOO_INPUT_SHARED;
//	    System.out.println("EmptyQ v1.0.0 by Capitalware Inc.");

		try {
			if ((qm == null) || (qm.isConnected == false)) {
				qm = MQHandle.connectarQM(qmgr);
				agent = new PCFMessageAgent(qm);
			}
			mq_queue = qm.accessQueue(queueParam, openOptions, null, null, null);

			MQGetMessageOptions getOptions = new MQGetMessageOptions();
			getOptions.options = MQC.MQGMO_NO_WAIT + MQC.MQGMO_FAIL_IF_QUIESCING + MQC.MQGMO_ACCEPT_TRUNCATED_MSG;
			MQMessage message;
			int cantMessagesRead = 0;
			while (loopAgain) {
				message = new MQMessage();
				try {
					mq_queue.get(message, getOptions, 1);
					cantMessagesRead++;
				} catch (MQException e) {
					if (e.completionCode == 1 && e.reasonCode == MQException.MQRC_TRUNCATED_MSG_ACCEPTED) {
						// Just what we expected!!
						cantMessagesRead++;
					} else {
						loopAgain = false;
						if (e.completionCode == 2 && e.reasonCode == MQException.MQRC_NO_MSG_AVAILABLE) {
							// Good, we are now done - no error!!
						} else {
							FacesContext.getCurrentInstance().addMessage(null,
									new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ",
											"EmptyQ: MQException: " + e.getLocalizedMessage()));
						}
					}
				}
			}
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", cantMessagesRead + " messages has been deleted"));
			NMTLogger.loguear(cantMessagesRead + " message/s has been deleted from qmgr " + qm.getName());
		} catch (MQException e1) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
					"Error: ", "EmptyQ: MQException: " + e1.getLocalizedMessage()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
		} catch (MQDataException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
		} finally {
			if (mq_queue != null) {
				try {
					mq_queue.close();
				} catch (MQException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}
	
	public String[] getQueuesFromQmgrFull() throws MQException {
		String[] listQ = null;
		PCFMessage[] responses = null;
		try {
			if ((qm == null) || (qm.isConnected == false)) {
				qm = MQHandle.connectarQM(qmgr);
				agent = new PCFMessageAgent(qm);
			}
			PCFMessage request = null;
			
			request = new PCFMessage(CMQCFC.MQCMD_INQUIRE_Q);
			/**
			 * You can explicitly set a queue name like "TEST.Q1" or use a wild card like
			 * "TEST.*"
			 */
			request.addParameter(CMQC.MQCA_Q_NAME, queue_name);
			// Add parameter to request only local queues
			request.addParameter(CMQC.MQIA_Q_TYPE, CMQC.MQQT_LOCAL);
			// Add parameter to request only queue name and current depth
			request.addParameter(CMQCFC.MQIACF_Q_ATTRS,
					new int[] { CMQC.MQCA_Q_NAME, CMQC.MQIA_CURRENT_Q_DEPTH, CMQC.MQIA_MAX_Q_DEPTH,
							CMQC.MQIA_OPEN_INPUT_COUNT, CMQC.MQIA_OPEN_OUTPUT_COUNT,
							CMQC.MQIA_MAX_MSG_LENGTH, CMQC.MQIA_DEF_PERSISTENCE });
			
			responses = agent.send(request);
			listQ = new String[responses.length];
			
			render = true;
			queues = new ArrayList<Queue>();
			for (int i = 0; i < responses.length; i++) {
				String queueName = "";
				if ( ((responses[i]).getCompCode() == CMQC.MQCC_OK) && ((responses[i]).getParameterValue(CMQC.MQCA_Q_NAME) != null) ) {
					queueName = responses[i].getStringParameterValue(CMQC.MQCA_Q_NAME).trim();
					if ((!queueName.contains("AMQ.")) && (!queueName.contains("SYSTEM."))){
						queue = new Queue();
						queue.name = queueName;
						int depth = responses[i].getIntParameterValue(CMQC.MQIA_CURRENT_Q_DEPTH);
						queue.curdepth = depth;
						if (depth > 0) {
							if (depth > 10) {
								queue.clase = "cell_danger";
							} else {
								queue.clase = "cell_warning";
							}
						}
						queue.maxdepth = responses[i].getIntParameterValue(CMQC.MQIA_MAX_Q_DEPTH);
						queue.inputc = responses[i].getIntParameterValue(CMQC.MQIA_OPEN_INPUT_COUNT);
						queue.outputc = responses[i].getIntParameterValue(CMQC.MQIA_OPEN_OUTPUT_COUNT);
						queue.maxmsgl = responses[i].getIntParameterValue(CMQC.MQIA_MAX_MSG_LENGTH);
						int defpsist = responses[i].getIntParameterValue(CMQC.MQIA_DEF_PERSISTENCE);
						// 1=SI
						if (defpsist == 1) {
							queue.defpsist = "YES";
						} else {
							queue.defpsist = "NO";
						}
						queues.add(queue);
					}
				}
			}
			
		} catch (MQException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MQDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
//			qm.disconnect();
		}
		// Remuevo los valores nulos del array para que no tire error
		listQ = Utils.removeNullValuesInArray(listQ);
		return listQ;
	}

	public void getQueuesInfoFromActiveMQ() throws JMSException, URISyntaxException {
		
		queues = new ArrayList<Queue>();
		ActiveMQConnectionFactory activeMQConnectionFactory = ActiveMQHandler.connectoToBroker(qmgr);
		Session session = ActiveMQHandler.getActiveMQSession(activeMQConnectionFactory, qmgr);

		Destination destination = session.createQueue(queue_name);
		queue = new Queue();
		queue.name = queue_name;
		int depth = ActiveMQHandler.getQueueSize(session, (javax.jms.Queue) destination);
		queue.curdepth = depth;
		if (depth > 0) {
			if (depth > 10) {
				queue.clase = "cell_danger";
			} else {
				queue.clase = "cell_warning";
			}
		}
		
		queues.add(queue);
		
	}
}
