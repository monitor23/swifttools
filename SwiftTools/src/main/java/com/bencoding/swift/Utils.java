package com.bencoding.swift;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Properties;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.primefaces.model.file.UploadedFile;

import com.bencoding.config.Variables;
import com.bencoding.utils.NMTLogger;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.headers.MQDataException;
import com.ibm.mq.headers.pcf.PCFMessageAgent;

public class Utils {
	
	public static String formatearGregorianCandelar(
			GregorianCalendar fechaCalendario) {
		Date newDate = fechaCalendario.getTime();
		SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String formateado = formato.format(newDate);
		return formateado;
	}
	
	// Estas rutinas se usan para convertir el valor que devuelve MQMessage de
	// msgid y correlId a hexa
	public static String byteArrayToHexString(byte[] byteArray) {
		int len = byteArray.length;
		StringBuffer hexStr = new StringBuffer();

		for (int j = 0; j < len; j++)
			hexStr.append(byteToHex((char) byteArray[j]));

		return hexStr.toString();
	}
	private static final String HEX = "0123456789ABCDEF";
	private static String byteToHex(char val) {
		int hi = (val & 0xF0) >> 4;
		int lo = (val & 0x0F);
		return "" + HEX.charAt(hi) + HEX.charAt(lo);
	}
	
	// Si no puede armarlo como XML lo devuelve tal cual entro
	public static String prettyFormat(String input, int indent) {
		try {
			Source inMessage = new StreamSource(new StringReader(input));
			StringWriter stringWriter = new StringWriter();
			StreamResult xmlOutput = new StreamResult(stringWriter);
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			transformerFactory.setAttribute("indent-number", indent);
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty("omit-xml-declaration","yes");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.transform(inMessage, xmlOutput);
			return xmlOutput.getWriter().toString();
		} catch (Exception e) {
			return input;
//				throw new RuntimeException(e); // simple exception handling, please
											// review it
		}
	}
	
	public static String[] removeNullValuesInArray(String[] sArray) {
	    List<String> list = new ArrayList<String>();
	    for(String s : sArray) {
	       if(s != null && s.length() > 0) {
	          list.add(s);
	       }
	    }
	    sArray = list.toArray(new String[list.size()]);
	    return sArray;
	}
	
	public static String prettyFormat(String input) throws JDOMException, IOException {
		Document doc = new SAXBuilder().build(new StringReader(input));
		String prettyxml = new XMLOutputter(Format.getPrettyFormat()).outputString(doc);
		return prettyxml;
	}
	
	public static BufferedReader getReaderFromUploadedFile(UploadedFile uploadedFile) throws IOException {
		
        InputStream stream = uploadedFile.getInputStream();
        InputStreamReader isReader = new InputStreamReader(stream);
        //Creating a BufferedReader object
        BufferedReader reader = new BufferedReader(isReader);
		return reader;
	}
	
	public static StringBuffer getStringBufferFromReader(BufferedReader reader) throws IOException {
		StringBuffer sb = new StringBuffer();
        String str;
        while((str = reader.readLine())!= null){
        		sb.append(str);
        }
        return sb;
	}
	
	public static byte[] getBytesFromUploadedFile(UploadedFile uploadedFile) {
		// If files content an empty line at the end i have to remove to get the correct LAU
        // I take the last bytes and check if they are 13,10 it means its an empty line
        byte[] emptyLine = {13, 10};
        byte[] fileContents = uploadedFile.getContent();
        byte[] lastLineOfContent = Arrays.copyOfRange(fileContents, fileContents.length - 2, fileContents.length);
        
        if (Arrays.equals(lastLineOfContent, emptyLine)) {
        	fileContents = Arrays.copyOfRange(fileContents,0, fileContents.length-2);
        }
        return fileContents;
	}
	
	public static MQQueueManager getQMConnetion(String qmgr) throws IOException, MQDataException, MQException {
		Properties prop = new Properties();
		prop.load(new FileInputStream(Variables.DirWork + "qmgrs.properties"));
//        (prop = new Properties()).load(Utils.class.getClassLoader().getResourceAsStream("config.properties"));
		String host = prop.getProperty(qmgr + ".hostname");
		int port = Integer.parseInt(prop.getProperty(qmgr + ".port"));
		String channel = prop.getProperty(qmgr + ".channel");
		MQEnvironment.hostname = host;
		MQEnvironment.port = port;
		MQEnvironment.channel = channel;
//		MQEnvironment.userID = "manu";
//		MQEnvironment.password = "";
		PCFMessageAgent agent = null;
		agent = new PCFMessageAgent();
        agent.connect(host,port,channel);
        MQQueueManager qm = new MQQueueManager(agent.getQManagerName());
        return qm;
	}
	
	public static String convertBufferedReaderToString(UploadedFile file) throws IOException {
		String output = "";
		BufferedReader br = Utils.getReaderFromUploadedFile(file);
		String line;
		while ((line = br.readLine()) != null) {
			output = String.valueOf(output) + line + "\n";
		}
		return output;
	}
	
	public static String convertBufferedReaderToStringWithCR(UploadedFile file) throws IOException {
		String output = "";
		BufferedReader br = Utils.getReaderFromUploadedFile(file);
		String line;
		while ((line = br.readLine()) != null) {
			output = String.valueOf(output) + line + "\r\n";
		}
		return output;
	}

	public static String[] obtenerCombosOrdenados(String archivo, String mascara) {
		try {
			Properties prop = new Properties();
			prop.load(new FileInputStream(Variables.DirWork + archivo));
//	        (prop = new Properties()).load(TestMessage.class.getClassLoader().getResourceAsStream("config.properties"));
			Enumeration e = prop.keys();
			String[] arr = new String[prop.size()];
			String valor = "";
			int i = 0;
			int cantValores = 0;
			// agrego primero los EG a un array para poder ordenarlos
			while (e.hasMoreElements()) {
				valor = e.nextElement().toString();
				if (valor.contains(mascara)) {
					arr[i] = prop.getProperty(valor);
					cantValores++;
				}
				i++;
			}
			String[] arrOrd = new String[cantValores];
			cantValores = 0;
			for (int j = 0; j < arr.length; j++) {
				valor = arr[j];
				if (valor != null) {
					arrOrd[cantValores] = arr[j].toString();
					cantValores++;
				}
			}
			Arrays.sort(arrOrd, String.CASE_INSENSITIVE_ORDER);
			return arrOrd;

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
			return null;
		}
	}
	
	public static String getMQProvider(String qmgr) {
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream(Variables.DirWork + "qmgrs.properties"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return prop.getProperty(qmgr + ".provider");
	}
	
	public static String readMessageFromFile(String path) {
		String message = "";
		File file = new File(path);
        if (file.exists()) {
        	try {
				message = new String(Files.readAllBytes(file.toPath()));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        return message;
	}
	
}
