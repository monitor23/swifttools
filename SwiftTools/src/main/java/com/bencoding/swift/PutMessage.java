package com.bencoding.swift;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.jms.JMSException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.primefaces.model.file.UploadedFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.bencoding.config.Variables;
import com.bencoding.utils.ActiveMQHandler;
import com.bencoding.utils.MQHandle;
import com.bencoding.utils.NMTLogger;
import com.ibm.mq.MQException;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.headers.MQDataException;

import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;


@SuppressWarnings("deprecation")
@ManagedBean(name="putMQ")
@ViewScoped


//TODO: No est� mostrando los errores en pantalla
public class PutMessage {
	
	private final char CR  = (char) 0x0D;
	
	private String keypart1 = "ABCD1234ABCD1234"; //ABCD1234ABCD1234 
	private String keypart2 = "ABCD1234ABCD1234"; //ABCD1234ABCD1234
	private String inQueue = "";
	private String inMessage = "";
	private String qmgr = "";
	private String[] listQueues = null;
	private String[] listMessages = null;
	private String block1="F01PTSQGBB0AAAA0003000003";
	private String block2="I999PTSQGBB0XAAAU3";
	private String block3="{108:swiftuser1}";
	private String block4_field20="test300";
	private String block4_field79="tests";
	private String inputFile = "basic";
	private String msgAmount = "";
	private String provider = "";
	
	
	public String getInMessage() {
		return inMessage;
	}

	public void setInMessage(String inMessage) {
		this.inMessage = inMessage;
	}

	public String[] getListMessages() {
		return listMessages;
	}

	public void setListMessages(String[] listMessages) {
		this.listMessages = listMessages;
	}

	public String getMsgAmount() {
		return msgAmount;
	}

	public void setMsgAmount(String msgAmount) {
		this.msgAmount = msgAmount;
	}

	public String getInputFile() {
		return inputFile;
	}

	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}

	public String getBlock1() {
		return block1;
	}

	public void setBlock1(String block1) {
		this.block1 = block1;
	}

	public String getBlock2() {
		return block2;
	}

	public void setBlock2(String block2) {
		this.block2 = block2;
	}

	public String getBlock3() {
		return block3;
	}

	public void setBlock3(String block3) {
		this.block3 = block3;
	}

	public String getBlock4_field20() {
		return block4_field20;
	}

	public void setBlock4_field20(String block4_field20) {
		this.block4_field20 = block4_field20;
	}

	public String getBlock4_field79() {
		return block4_field79;
	}

	public void setBlock4_field79(String block4_field79) {
		this.block4_field79 = block4_field79;
	}

	public String[] getListQueues() {
		return listQueues;
	}

	public void setListQueues(String[] listQueues) {
		this.listQueues = listQueues;
	}

	public String getInQueue() {
		return inQueue;
	}

	public void setInQueue(String inQueue) {
		this.inQueue = inQueue;
	}

	public String getQmgr() {
		return qmgr;
	}

	public void setQmgr(String qmgr) {
		this.qmgr = qmgr;
	}

	private UploadedFile uploadedFile = null;

	
	public String getKeypart1() {
		return keypart1;
	}

	public void setKeypart1(String keypart1) {
		this.keypart1 = keypart1;
	}

	public String getKeypart2() {
		return keypart2;
	}

	public void setKeypart2(String keypart2) {
		this.keypart2 = keypart2;
	}

	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}
	
	public void getSampleMessages(AjaxBehaviorEvent event) throws MQException{
		
		List<String> results = new ArrayList<String>();


		File[] files = new File(Variables.DirWork + "/Samples/MT").listFiles();
		//If this pathname does not denote a directory, then listFiles() returns null. 

		for (File file : files) {
		    if (file.isFile()) {
		        results.add(file.getName());
		    }
		}
		listMessages = results.toArray(new String[results.size()]);
		
	}
	
	public void getQmgrQueues(AjaxBehaviorEvent event) throws MQException, FileNotFoundException, IOException{
		String[] listQ = null;
		// now that we have IBM MQ and ActiveMQ I have to connect with 2 differet ways.
		provider = Utils.getMQProvider(qmgr);
		switch (provider.toUpperCase()) {
		case "IBM":
				try {
					MQQueueManager qm = MQHandle.connectarQM(qmgr);
					listQ = MQHandle.getQueuesFromQmgr(qm);
					listQueues = listQ;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Could not connect to queue manager"));
					NMTLogger.loguear(e.toString());
				}
			break;

		case "APACHE":
			try {
				listQueues = ActiveMQHandler.getQueueList(qmgr);
			} catch (JMSException | URISyntaxException e) {
				// TODO Auto-generated catch block
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Could not connect to ActiveMQ Broker"));
				e.printStackTrace();
			}
			break;
		default:
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Provider not found"));
			break;
		}
		
		
	}
	
	public String[] getQmgrs() {
		String qmgrs[] = getAGIQmgrs("qmgrs.properties", ".name");
		if (qmgrs != null) {
			for (int j = 0; j < qmgrs.length; j++) {
				qmgrs[j].toString();
			}
		}
		return qmgrs;
	}
	
	public void putMessage() throws InvalidKeyException, NoSuchAlgorithmException {
		
		File file = null;
		String inMessage = "";
		boolean readMessageOK = false;
		
		switch (inputFile) {
		case "basic":
	        file = new File(Variables.DirWork + "Samples/agi-basic-message-sample.xml");
	        if (file.exists()) {
	        	readMessageOK = true;
	        	try {
					inMessage = new String(Files.readAllBytes(file.toPath()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
			break;
		case "nr":
//	        file = new File(getClass().getClassLoader().getResource("message_samples/agi-nr-message-sample.xml").getFile());
			file = new File(Variables.DirWork + "Samples/agi-nr-message-sample.xml");
	        if (file.exists()) {
	        	readMessageOK = true;
	        	try {
					inMessage = new String(Files.readAllBytes(file.toPath()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
			break;
		case "choose":
			if ((uploadedFile != null) && (uploadedFile.getFileName() != null)) {
				try {
					inMessage = Utils.convertBufferedReaderToString(uploadedFile);
					readMessageOK=true;
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        inMessage = inMessage.substring(0, inMessage.length() - 1);
	        
			} else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning:", "Please select a file"));
			}
	        
			break;
		}
		
		if(readMessageOK) {
			
			MQQueueManager qm;
			try {
				// Build the string message
				String key = keypart1 + keypart2;
	            String LAUData = new String("");
	            
	            String version = getSubstringFromMessage(inMessage,"<mq:property name=\"Version\">","</mq:property>");
	            String service = getSubstringFromMessage(inMessage,"<mq:property name=\"Service\">","</mq:property>");
	            String sender = getSubstringFromMessage(inMessage,"<mq:property name=\"Sender\">","</mq:property>");
	            String receiver = getSubstringFromMessage(inMessage,"<mq:property name=\"Receiver\">","</mq:property>");
	            String primitiveType = getSubstringFromMessage(inMessage,"<mq:property name=\"PrimitiveType\">","</mq:property>");
	            String msgType = getSubstringFromMessage(inMessage,"<mq:property name=\"MsgType\">","</mq:property>");
	            String msgRef = getSubstringFromMessage(inMessage,"<mq:property name=\"MsgRef\">","</mq:property>");
	            String additionalInfo = getSubstringFromMessage(inMessage,"<mq:property name=\"AdditionalInfo\">","</mq:property>");
	            String possibleDuplicate = getSubstringFromMessage(inMessage,"<mq:property name=\"PossibleDuplicate\">","</mq:property>");
	            String technicalAckRequired = getSubstringFromMessage(inMessage,"<mq:property name=\"TechnicalAckRequired\">","</mq:property>");
	            String notificationRequired = getSubstringFromMessage(inMessage,"<mq:property name=\"NotificationRequired\">","</mq:property>");
	            
	            LAUData = LAUData.concat(version);
	            LAUData = LAUData.concat(service);
	            LAUData = LAUData.concat(sender);
	            LAUData = LAUData.concat(receiver);
	            LAUData = LAUData.concat(primitiveType);
	            LAUData = LAUData.concat(msgType);
	            LAUData = LAUData.concat(msgRef);
	            LAUData = LAUData.concat(additionalInfo); 
	            LAUData = LAUData.concat(possibleDuplicate);
	            LAUData = LAUData.concat(notificationRequired);
	            LAUData = LAUData.concat(technicalAckRequired);
				
	            //final part is the payload used for HMAC and HMAC2 calculation
	            String payload = "";
	            payload = inMessage.substring(inMessage.indexOf("<n1:Document"),inMessage.indexOf("</n1:Document")+14);
            	payload = payload.replace("\r", "");
            	LAUData = LAUData.concat(payload);
            	// maybe this is not necessary because i am already doing the replace in the payload, but just in case i leave it for now
	            LAUData = LAUData.replace("\r", "");
	    		String lau = CLAU.ComputeLAU(key, LAUData);
	    		int msgsToSend = Integer.parseInt(msgAmount);
				switch (provider.toUpperCase()) {
				case "IBM":
					qm = MQHandle.connectarQM(qmgr);
					MQMessage msg = new MQMessage();
		            try {
						msg.setStringProperty("MsgType", msgType);
						msg.setStringProperty("TechnicalAckRequired", technicalAckRequired);
						msg.setStringProperty("HMAC", lau);
						msg.setStringProperty("SignatureRequired", "true");
						msg.setStringProperty("MsgRef", msgRef);
						msg.setStringProperty("PrimitiveType", primitiveType);
						msg.setStringProperty("BIC8", "BANKEU2A");
						msg.setStringProperty("Service", service);
						msg.setStringProperty("HMACKeyId", "A");
						msg.setStringProperty("TestCaseId", "TC04-02");
						msg.setStringProperty("Sender", sender);
						msg.setStringProperty("NotificationRequired", notificationRequired);
						msg.setStringProperty("Receiver", receiver);
						msg.setStringProperty("Version", version);
						msg.setStringProperty("PossibleDuplicate", possibleDuplicate);
						
						msg.characterSet = 1208;
						MQPutMessageOptions pmo = new MQPutMessageOptions();
						msg.write(payload.getBytes());
						for (int x=0;x<msgsToSend;x++) {
							qm.put(inQueue, msg, pmo);
						}
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", " Message/s successfully put to queue!!"));
						NMTLogger.loguear("Message/s successfully put to queue!!");
						NMTLogger.loguear("Payload  sent \r\n "+ payload);
						qm.close();
						qm.disconnect();
		            }catch (Exception e) {
		            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Input message is not valid"));
		            	NMTLogger.loguear(e.toString());
		            }
					break;
							
				case "APACHE":
					try {
						ActiveMQConnectionFactory activeMQconnectionFactory = ActiveMQHandler.connectoToBroker(qmgr); 
		                // Create a Connection
		                Connection connection = activeMQconnectionFactory.createConnection();
		                connection.start();
		                // Create a Session
		                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
		                // Create the destination (Topic or Queue)
		                Destination destination = session.createQueue(inQueue);
		                // Create a MessageProducer from the Session to the Topic or Queue
		                MessageProducer producer = session.createProducer(destination);
		                producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
		                // Create a messages
		                // String text = "Hello world! From: " + Thread.currentThread().getName() + " : " + this.hashCode();
		                BytesMessage messageForActiveMQ=session.createBytesMessage();
		                messageForActiveMQ.writeBytes(payload.getBytes());
		                //  TextMessage msg = session.createTextMessage(payload);
		                messageForActiveMQ.setStringProperty("MsgType", msgType);
		                messageForActiveMQ.setStringProperty("TechnicalAckRequired", technicalAckRequired);
		                messageForActiveMQ.setStringProperty("HMAC", lau);
		                messageForActiveMQ.setStringProperty("SignatureRequired", "true");
		                messageForActiveMQ.setStringProperty("MsgRef", msgRef);
		                messageForActiveMQ.setStringProperty("PrimitiveType", primitiveType);
						//msg.setStringProperty("BIC8", "BANKEU2A");
		                messageForActiveMQ.setStringProperty("Service", service);
		                messageForActiveMQ.setStringProperty("HMACKeyId", "A");
						//msg.setStringProperty("TestCaseId", "TC04-02");
		                messageForActiveMQ.setStringProperty("Sender", sender);
		                messageForActiveMQ.setStringProperty("NotificationRequired", notificationRequired);
		                messageForActiveMQ.setStringProperty("Receiver", receiver);
		                messageForActiveMQ.setStringProperty("Version", version);
		                messageForActiveMQ.setStringProperty("PossibleDuplicate", possibleDuplicate);
		                
		                // Tell the producer to send the message
//		                System.out.println("Sent message: "+messageForActiveMQ);
		                for (int x=0;x<msgsToSend;x++) {
							producer.send(messageForActiveMQ);
						}
		                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", " Message/s successfully put to queue!!"));
						NMTLogger.loguear("Message/s successfully put to queue!!");
						NMTLogger.loguear("Payload  sent \r\n "+ payload);
		                // Clean up
		                session.close();
		                connection.close();
		            } catch (JMSException e) {
						// TODO Auto-generated catch block
		            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Error while putting message to activeMQ"));
		            	NMTLogger.loguear(e.toString());
						e.printStackTrace();
					}
					
					break;
				default:
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Provider not found"));
					break;
				}
	    		
			} catch (IOException | MQException e1) {
				// TODO Auto-generated catch block
				NMTLogger.loguear(e1.toString());
			}
		}
	}
	
	public void putMT199Message(){
		
		String message = "";
		File file = null;
		if (inMessage.equals("Form")) {
			// build the string message using the values from the form
			message = "{1:" + block1 + "}{2:" + block2 + "}{3:" + block3 + "}{4:" + CR + "\n:20:" + block4_field20 + CR + "\n:79:" + block4_field79 + CR + "\n-}";
		} else {
			file = new File(Variables.DirWork + "Samples/MT/" + inMessage);
	        if (file.exists()) {
	        	try {
	        		message = new String(Files.readAllBytes(file.toPath()));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
	        }
		}
		MQQueueManager qm;
		try {
			qm = Utils.getQMConnetion(qmgr);
			MQMessage msg = new MQMessage();
			msg.characterSet = 1208;
			MQPutMessageOptions pmo = new MQPutMessageOptions();
			msg.write(message.getBytes());	
			qm.put(inQueue, msg, pmo);
			NMTLogger.loguear("MQ199 message sent: " + message.replace("\n", ""));
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", "Message successfully put to queue!!"));
			qm.close();
			qm.disconnect();
		} catch (IOException | MQDataException | MQException e) {
			// TODO Auto-generated catch block
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", e.toString()));
			NMTLogger.loguear(e.toString());
		}
		
	}

	public static String stringToHex(String str) {
	    StringBuffer sb = new StringBuffer();
	    //Converting string to character array
	    char ch[] = str.toCharArray();
	    for(int i = 0; i < ch.length; i++) {
	       String hexString = Integer.toHexString(ch[i]);
	       sb.append(hexString);
	    }
	    String result = sb.toString();
	    return result;
	}
	
	public static String readFile(String path) {
		String value = "";
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc = docBuilder.parse(new File(path));
	        doc.getDocumentElement().normalize();
	        Element eParemtros = (Element)doc.getElementsByTagName("parametros").item(0);
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	// Me traigo del .bar todas las propiedaeds que se pueden sobreescribir
		catch (SAXException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
		}
        return value;
	}
	
	public static Document getXMLDocument(String path) {
		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder;
        Document doc = null;
		try {
			docBuilder = docBuilderFactory.newDocumentBuilder();
			doc = docBuilder.parse(new File(path));
	        doc.getDocumentElement().normalize();
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
		}
		return doc;
	}
	
	public static String getSubstringFromMessage(String s, String searchStart, String searchEnd) {
		String output = "";
		int lenght = searchStart.length();
		if (s.contains(searchStart) && s.contains(searchEnd)) {
			output = s.substring(s.indexOf(searchStart)+lenght);
			output = output.substring(0,output.indexOf(searchEnd));
		}
		return output; 
	}
	
	public static String[] getAGIQmgrs(String archivo, String mascara) {
		try {
			Properties prop = new  Properties();
			prop.load(new FileInputStream(Variables.DirWork + archivo));
//	        (prop = new Properties()).load(TestMessage.class.getClassLoader().getResourceAsStream("config.properties"));
			Enumeration e = prop.keys();
			String[] arr = new String[prop.size()];
			String valor = "";
			int i = 0;
			int cantValores = 0;
			// agrego primero los EG a un array para poder ordenarlos
			while (e.hasMoreElements()) {
				valor = e.nextElement().toString();
				if (valor.contains(mascara)) {
					// Only add qmgrs that are use for AGI
					String p = prop.getProperty(valor);
					if(prop.getProperty(p+".isForAGI").equalsIgnoreCase("true")) {
						arr[i] = p;
						cantValores++;
					}
				}
				i++;
			}
			String[] arrOrd = new String[cantValores];
			cantValores = 0;
			for (int j = 0; j < arr.length; j++) {
				valor = arr[j];
				if (valor != null) {
					arrOrd[cantValores] = arr[j].toString();
					cantValores++;
				}
			}
			Arrays.sort(arrOrd, String.CASE_INSENSITIVE_ORDER);
			return arrOrd;

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
			return null;
		}
	}
}
