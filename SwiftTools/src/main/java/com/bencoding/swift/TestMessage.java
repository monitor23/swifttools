package com.bencoding.swift;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.jms.BytesMessage;
import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.primefaces.model.file.UploadedFile;

import com.bencoding.config.Variables;
import com.bencoding.utils.ActiveMQHandler;
import com.bencoding.utils.MQHandle;
import com.bencoding.utils.NMTLogger;
import com.ibm.mq.MQC;
import com.ibm.mq.MQEnvironment;
import com.ibm.mq.MQException;
import com.ibm.mq.MQGetMessageOptions;
import com.ibm.mq.MQMessage;
import com.ibm.mq.MQPutMessageOptions;
import com.ibm.mq.MQQueue;
import com.ibm.mq.MQQueueManager;
import com.ibm.mq.constants.CMQC;
import com.ibm.mq.constants.MQConstants;
import com.ibm.mq.headers.pcf.PCFMessageAgent;
import com.ibm.mq.pcf.MQCFH;
import com.ibm.mq.pcf.MQCFIN;
import com.ibm.mq.pcf.MQCFST;
import com.ibm.mq.pcf.PCFAgent;
import com.ibm.mq.pcf.PCFParameter;

@SuppressWarnings("deprecation")
@ManagedBean(name="test")
@ViewScoped

public class TestMessage {
	
	private String qmgr;
	private String inMessage;
	private String outMessage;
	private String[] listQueues = null;
	private String[] listSampleFiles = null;
	private boolean render=false;
	private String inQueue;
	private String outQueue;
	private String loadingBar = "../resources/images/loading-bar.gif";
	private UploadedFile uploadedFile = null;
	private String directory = Variables.DirWork + "/NETLINK_ITB";
	private List<String> filesNames;
	private String sampleFileName;
	private String inputFile = "sample";
	
	
	public String getInputFile() {
		return inputFile;
	}


	public void setInputFile(String inputFile) {
		this.inputFile = inputFile;
	}


	public String getSampleFileName() {
		return sampleFileName;
	}


	public void setSampleFileName(String sampleFileName) {
		this.sampleFileName = sampleFileName;
	}


	public String[] getListSampleFiles() {
//		listSampleFiles = {"Volvo", "BMW", "Ford", "Mazda"};
		return listSampleFiles;
	}


	public void setListSampleFiles(String[] listSampleFiles) {
		this.listSampleFiles = listSampleFiles;
	}


	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}


	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}


	public String getLoadingBar() {
		return loadingBar;
	}


	public void setLoadingBar(String loadingBar) {
		this.loadingBar = loadingBar;
	}

	private int number1;
	
	public int getNumber1() {
		return number1;
	}


	public void setNumber1(int number1) {
		this.number1 = number1;
	}


	public String getQmgr() {
		return qmgr;
	}


	public void setQmgr(String qmgr) {
		this.qmgr = qmgr;
	}


	public String getInMessage() {
		return inMessage;
	}


	public void setInMessage(String inMessage) {
		this.inMessage = inMessage;
	}


	public String getOutMessage() {
		return outMessage;
	}


	public void setOutMessage(String outMessage) {
		this.outMessage = outMessage;
	}


	public String[] getListQueues() {
		return listQueues;
	}


	public void setListQueues(String[] listQueues) {
		this.listQueues = listQueues;
	}


	public boolean isRender() {
		return render;
	}


	public void setRender(boolean render) {
		this.render = render;
	}


	public String getInQueue() {
		return inQueue;
	}


	public void setInQueue(String inQueue) {
		this.inQueue = inQueue;
	}


	public String getOutQueue() {
		return outQueue;
	}


	public void setOutQueue(String outQueue) {
		this.outQueue = outQueue;
	}

	
	public String[] getQmgrs() {
		String qmgrs[] = Utils.obtenerCombosOrdenados("qmgrs.properties", ".name");
		if (qmgrs != null) {
			for (int j = 0; j < qmgrs.length; j++) {
				qmgrs[j].toString();
			}
		}
		return qmgrs;
	}
	
	public void buildListSampleFilesCombo(AjaxBehaviorEvent event) {
		
		filesNames = new ArrayList<String>();
		walk(directory);
	    listSampleFiles = filesNames.toArray(new String[filesNames.size()]);
	    System.out.println(listSampleFiles[0]);
	}
	
	public void putMessage() throws IOException{
		
		String message = "";
		if(inputFile.equals("sample")) {
			String path = sampleFileName;
			message = Utils.readMessageFromFile(path);
		} else { //If its not sample is an upload file 
			uploadedFile.getContent();
		}
		byte[] messageBytes = message.getBytes();
		
		String provider = Utils.getMQProvider(qmgr);
		switch (provider.toUpperCase()) {
		case "IBM":
			// Comment all the parts use to receive a response
			MQQueueManager qm = null;
			try{
		        qm = MQHandle.connectarQM(qmgr);
		        
				MQMessage msg = new MQMessage();
				msg.encoding = 273;
				msg.characterSet = 1208;
				msg.format = MQC.MQFMT_STRING;
//				msg.replyToQueueName = outQueue;
				
				MQPutMessageOptions pmo = new MQPutMessageOptions();
				msg.write(messageBytes);
//				byte[] messageBytes = uploadedFile.getContent();
				qm.put(inQueue, msg, pmo);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO: ", "Message successfully put!"));
				
			}		
			catch (Exception e){
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", e.toString()));
				NMTLogger.loguear(e.toString());
			}
			finally {
				try {
					qm.close();
					qm.disconnect();
				} catch (MQException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
			
			break;

		case "APACHE":
			try {
				ActiveMQConnectionFactory activeMQconnectionFactory = ActiveMQHandler.connectoToBroker(qmgr); 
                // Create a Connection
                Connection connection = activeMQconnectionFactory.createConnection();
                connection.start();
                // Create a Session
                Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
                // Create the destination (Topic or Queue)
                Destination destination = session.createQueue(inQueue);
                // Create a MessageProducer from the Session to the Topic or Queue
                MessageProducer producer = session.createProducer(destination);
                producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
                BytesMessage messageForActiveMQ=session.createBytesMessage();
                messageForActiveMQ.writeBytes(messageBytes);
				producer.send(messageForActiveMQ);
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", " Message/s successfully put to queue!!"));
				NMTLogger.loguear("Message/s successfully put to queue!!");
				NMTLogger.loguear("Payload  sent \r\n "+ uploadedFile.getContent());
                // Clean up
                session.close();
                connection.close();
            } catch (JMSException e) {
				// TODO Auto-generated catch block
            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Error while putting message to activeMQ"));
            	NMTLogger.loguear(e.toString());
				e.printStackTrace();
			}
			break;
		}
		
	}
	
	@SuppressWarnings("deprecation")
	public void getQmgrQueues(AjaxBehaviorEvent event) throws MQException{
		
		listQueues = MQHandle.getQmgrQueuesFromProvider(qmgr);
	}
	
	public void walk( String path ) {
		
        File root = new File( path );
        File[] list = root.listFiles();

        for ( File f : list ) {
            if ( f.isDirectory() ) {
                walk( f.getAbsolutePath() );
//                System.out.println( "Dir:" + f.getAbsoluteFile() );
            }
            else {
            	String pathToReplace = directory.replace("/","\\");
            	String fileName =  f.getAbsoluteFile().toString().replace(pathToReplace, "");
            	filesNames.add(fileName);
//                System.out.println( fileName );
            }
        }
        
    }
}
