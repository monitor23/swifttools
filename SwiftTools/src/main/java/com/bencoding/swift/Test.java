package com.bencoding.swift;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Base64;
import java.util.Hashtable;
import java.util.Properties;

import org.apache.http.client.methods.CloseableHttpResponse;

import com.ibm.mq.*;
import com.ibm.mq.constants.MQConstants;


public class Test {

    public static void main(String[] args) {
    
        String filename = "C:\\Users\\ee49884\\\\Desktop\\File-Manu.txt";
        //Read data from file
        File file = null;
        FileInputStream fis = null;
        BufferedInputStream bis = null;
        try {
        	file = new File(filename);
            fis = new FileInputStream(file);               
            bis = new BufferedInputStream(fis);
            System.out.println("Processing File [" + filename + "]...");
            byte[] DigestValueBinary = {0};
            String DigestValueString = "";
            DigestValueBinary = getDigestSHA256(bis);
//            DigestValueString = base64Encode(DigestValueBinary);
            
            String encoded = Base64.getEncoder().encodeToString(DigestValueBinary);
        	
        	System.out.println(encoded);
        	
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
    }
    
    public static byte[] getDigestSHA256(BufferedInputStream stream) throws Exception {

        // Get SHA-256 digester
        MessageDigest msgDigest = MessageDigest.getInstance("SHA-256");
        //Get the payload in the digester
        int bytesRead = -1;
        byte[] buffer = new byte[10000];
        while ((bytesRead = stream.read(buffer)) > 0) msgDigest.update(buffer, 0, bytesRead);   
        //Get the digest value
        return(msgDigest.digest());

    }
    
    public static String readFile(String fileName) throws IOException {
    	BufferedReader reader = new BufferedReader(new FileReader(fileName));
    	StringBuilder stringBuilder = new StringBuilder();
    	String line = null;
    	String ls = System.getProperty("line.separator");
    	while ((line = reader.readLine()) != null) {
    		stringBuilder.append(line);
    		stringBuilder.append(ls);
    	}
    	// delete the last new line separator
    	stringBuilder.deleteCharAt(stringBuilder.length() - 1);
    	reader.close();

    	String content = stringBuilder.toString();
    	return content;
    }
    
    private static String bytesToHex(byte[] hash) {
        StringBuilder hexString = new StringBuilder(2 * hash.length);
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(0xff & hash[i]);
            if(hex.length() == 1) {
                hexString.append('0');
            }
            hexString.append(hex);
        }
        return hexString.toString();
    }
    
    private static String readFileAsBinary(String fileName) {
    	String output = "";
    	try {
    	    // create a reader
    	    FileInputStream fis = new FileInputStream(new File(fileName));
    	    
    	    // specify UTF_16 characer encoding
    	    InputStreamReader reader = new InputStreamReader(fis, StandardCharsets.UTF_8);

    	    // read one byte at a time
    	    int ch;
    	    
    	    while ((ch = reader.read()) != -1) {
    	    	output += (char) ch;
//    	        System.out.print((char) ch);
    	    }
    	    // close the reader
    	    reader.close();
    	    
    	} catch (IOException ex) {
    	    ex.printStackTrace();
    	}
    	return output;
    }
    
    public static String toHexString(byte[] hash)
    {
        // Convert byte array into signum representation 
        BigInteger number = new BigInteger(1, hash); 
  
        // Convert message digest into hex value 
        StringBuilder hexString = new StringBuilder(number.toString(16)); 
  
        // Pad with leading zeros
        while (hexString.length() < 32) 
        { 
            hexString.insert(0, '0'); 
        } 
  
        return hexString.toString(); 
    }
}