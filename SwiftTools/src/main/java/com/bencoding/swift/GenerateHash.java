package com.bencoding.swift;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.file.UploadedFile;

import com.bencoding.utils.NMTLogger;

@ManagedBean(name="hash")
@ViewScoped

public class GenerateHash {
	
	private String mensaje = "Welcome to Swift tools!!";
	
	private String keypart1 = ""; //"Abcdef0123456789";
	private String keypart2 = ""; //"Abcdef0123456789";
	private String newMessage = "";
	private boolean saveToFileEnable = false;
	private StreamedContent file;
	private byte[] finalMessage;
	
	private UploadedFile uploadedFile = null;
	
	public UploadedFile getUploadedFile() {
		return uploadedFile;
	}

	public void setUploadedFile(UploadedFile uploadedFile) {
		this.uploadedFile = uploadedFile;
	}

	public StreamedContent getFile() {
		return file;
	}

	public void setFile(StreamedContent file) {
		this.file = file;
	}

	public boolean isSaveToFileEnable() {
		return saveToFileEnable;
	}

	public void setSaveToFileEnable(boolean saveToFileEnable) {
		this.saveToFileEnable = saveToFileEnable;
	}

	FacesContext context = FacesContext.getCurrentInstance();
	
	public String getKeypart1() {
		return keypart1;
	}

	public void setKeypart1(String keypart1) {
		this.keypart1 = keypart1;
	}

	public String getKeypart2() {
		return keypart2;
	}

	public void setKeypart2(String keypart2) {
		this.keypart2 = keypart2;
	}

	public String getNewMessage() {
		return newMessage;
	}

	public void setNewMessage(String newMessage) {
		this.newMessage = newMessage;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	public void generateHash() {
		
		String key = keypart1 + keypart2;
		String inMessage = new String();
		if (uploadedFile.getFileName() != null) {
			try {
				inMessage = Utils.convertBufferedReaderToStringWithCR(uploadedFile);
		        inMessage = inMessage.substring(0, inMessage.length() - 2); // the previous function adds \r\n at the end of the message and i need to remove it
				String MDG_Block = hmacSha1(inMessage, key);
		        String hash = "{S:" + "{MDG:" + MDG_Block + "}}";
		        newMessage = inMessage + hash;
		        finalMessage = newMessage.getBytes(); // I use finalMessage in the download view
		        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", "Hash successfully generated!"));
		        FileDownloadView("lauMessage_ForJRE.txt");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				NMTLogger.loguear(e.toString());
			}
	        
		} else {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning:", "Please select a file to upload"));
        }
		
	}
	
	public static String hmacSha1(String sMessage, String key) {
        try {
            byte[] rawHmac = getHmac(key, sMessage.getBytes());
            // Convert raw bytes to Hex
            byte[] hexBytes = new Hex().encode(rawHmac);
            //  Covert array of Hex bytes to a String
            return new String(hexBytes, "UTF-8").toUpperCase();
        } catch (Exception e) {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Failed generating hash code"));
            throw new RuntimeException(e);
        }
    }
	
	/*
	public void FileDownloadView2(String name) {
		
		InputStream stream = new ByteArrayInputStream( finalMessage );
		file = new DefaultStreamedContent(stream, "text/plain", name);
		saveToFileEnable = true;

    } */
	
	public void FileDownloadView(String name) {
		saveToFileEnable = true;
		InputStream stream = new ByteArrayInputStream( finalMessage );
		String timestamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
		String newFileName = "LAU_" + timestamp + "_" + uploadedFile.getFileName();
        file = DefaultStreamedContent.builder()
                .name(newFileName)
                .contentType("text/plain")
                .stream(() -> stream)
                .build();
    }

	public void generateLAUxml() throws IOException {
		
		String key = keypart1 + keypart2;
		if (uploadedFile.getFileName() != null) {
            BufferedReader reader = Utils.getReaderFromUploadedFile(uploadedFile);
            StringBuffer sb = Utils.getStringBufferFromReader(reader);
            byte[] fileContents = Utils.getBytesFromUploadedFile(uploadedFile);
            	
    		try {
                byte[] rawHmac = getHmac(key, fileContents);
                byte[] slice = Arrays.copyOfRange(rawHmac, 0, 16);
        		String lau = Base64.encodeBase64String(slice);
        		String s = sb.toString();
        		saveToFileEnable = true;
        		newMessage = Utils.prettyFormat(s);
        		
        		int fileLenght = fileContents.length;
        		int lauLenght = lau.length(); // It should always be 24
        		String totalLenght = String.valueOf(fileLenght+lauLenght);
        		
        		// Complete the lenght with "0" to fill 6 characters lenght
        		for(int i=totalLenght.length();i<6;i++){
        			totalLenght = "0" + totalLenght;
        		}
        		
        		char pref = 0x1f;
        		newMessage = pref + totalLenght + lau + newMessage;
        		
        		ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
        		outputStream.write( pref );
        		outputStream.write( totalLenght.getBytes() );
        		outputStream.write( lau.getBytes() );
        		outputStream.write( fileContents );

        		finalMessage = outputStream.toByteArray( );
        		
        		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info:", "LAU successfully generated!"));
        		FileDownloadView("LAU_" + uploadedFile.getFileName());
            } catch (Exception e) {
            	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error:", "Failed generating hash code"));
//                throw new RuntimeException(e);
            }
            
        } else {
        	FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "Warning:", "Please select a file to upload"));
        }
	}

	public static byte[] getHmac(String key, byte[] messageBytes) throws NoSuchAlgorithmException, InvalidKeyException {
		// Get an hmac_sha1 key from the raw key bytes
		byte[] keyBytes = key.getBytes();           
		SecretKeySpec signingKey = new SecretKeySpec(keyBytes, "HmacSHA1");

		// Get an hmac_sha1 Mac instance and initialize with the signing key
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(signingKey);

		// Compute the hmac on input data bytes
		byte[] rawHmac = mac.doFinal(messageBytes);
		return rawHmac;
	}
	
}
