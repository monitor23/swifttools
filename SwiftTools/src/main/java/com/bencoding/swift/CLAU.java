package com.bencoding.swift;

import java.util.*;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

public class CLAU {

	static public  final String ALG_HMAC_SHA256 = "HmacSHA256"  ;
	static public  final String UTF_8    = "UTF-8"; 
	static public  final String US_ASCII = "US-ASCII";
	static final int MSG_SIZE = 12000;
	
	static String errorText = "";

	//=======================================================
	// Return the last error detected by this class
	//=======================================================
	
	String getLastError() {
		return(errorText);
	}
	
	//=======================================================
	// Convert a string with hex characters into its binary equivalent
	//=======================================================
	
	static byte[] hexStringToByteArray(String s) {
	    byte[] b = new byte[s.length() / 2];
	    for (int i = 0; i < b.length; i++) {
	      int index = i * 2;
	      int v = Integer.parseInt(s.substring(index, index + 2), 16);
	      b[i] = (byte) v;
	    }
	    return b;
	  }

	//=======================================================
	//Build and empty HMAC byte value
	//=======================================================
	
	 static byte[] getEmptyHmac() throws Exception {
		
		byte[] buffer = new byte[64];
		return buffer;
	}
	
	//=======================================================
	//convert binary bytes to hexadecimal string
	//=======================================================
	
	String getHexString(byte[] b, char format) throws Exception {
		
	  String result = "";

	  if(format == 'P')
	  {
		  for (int i=0; i < b.length; i++) {
			  result += (Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 )).toUpperCase();
			  if ((i+1) % 4 == 0) result += "-"; else result += " ";
		  }
		  result = result.substring(0, result.length()-1);
	  } else{
		  for (int i=0; i < b.length; i++) {
			  result += (Integer.toString( ( b[i] & 0xff ) + 0x100, 16).substring( 1 )).toUpperCase();
		  }
	  }
	  return result;
	}

	//=======================================================
	//compute HMAC-SHA-256
	//=======================================================
	
	static byte[] getHmacSHA256(String data, byte[] key) throws Exception {
		
		//we still need to return something - empty LAU will encoded as a bunch of 0 chars ...
		if (key.toString().isEmpty()) return getEmptyHmac();

		//Compute HMAC-SHA-256
        Mac m = Mac.getInstance(ALG_HMAC_SHA256);
        SecretKeySpec keyspec = new SecretKeySpec(key, ALG_HMAC_SHA256); 
        m.init(keyspec);        

        byte[] dataBytes = data.getBytes();
		m.update(dataBytes, 0, dataBytes.length);    
        
        return m.doFinal();
    }
	
	//=======================================================
	// main routine computing LAU value
	//=======================================================
	
	public static String ComputeLAU(String LAUKey, String Data) {

		byte[] LAUValueBinary = {0};
		byte[] LAUKeyBinary = {0};
		String LAUValueHex = "";

		errorText = "";
		
		//Convert key expressed as an hexadecimal string to hexadecimal binary values
		LAUKeyBinary = hexStringToByteArray(LAUKey);
			
		//Compute MAC-SHA-256
		try {
			LAUValueBinary = getHmacSHA256(Data, LAUKeyBinary);
		} catch (Exception e) {
			errorText = "Cannot compute HMAC-SHA-256";
			e.printStackTrace();
		}
		
		//base-64 encode the binary value
		LAUValueHex = Base64.getEncoder().encodeToString(LAUValueBinary);
	
		// return result
		return(LAUValueHex);
	}
}
