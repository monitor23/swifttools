package com.bencoding.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.configuration.PropertiesConfigurationLayout;

import com.bencoding.config.Variables;
import com.bencoding.utils.NMTLogger;

@SuppressWarnings("deprecation")
@ManagedBean(name="config_qmgrs")
@ViewScoped

public class ConfQmgrs {
	
	private String qmgrName = "";
	private String server = "";
	private int port;
	private String channel = "";
	private String username ="";
	private String password = "";
	private String isAGI = "";

	
	public String getIsAGI() {
		return isAGI;
	}
	public void setIsAGI(String isAGI) {
		this.isAGI = isAGI;
	}
	public String getQmgrName() {
		return qmgrName;
	}
	public void setQmgrName(String qmgrName) {
		this.qmgrName = qmgrName;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.channel = channel;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public void addNewQmgr() {
		
		try {
			File file = new File(Variables.DirWork + "/qmgrs.properties");
			PropertiesConfiguration config = new PropertiesConfiguration();
	        PropertiesConfigurationLayout layout = new PropertiesConfigurationLayout(config);
	        layout.load(new InputStreamReader(new FileInputStream(file)));
	        
	     // Verifico si ya existe el qmgr en el properties.
	        Boolean existe = config.containsKey(qmgrName +".name");
			if (!existe){
				config.addProperty("######" + qmgrName + "######","");
				config.addProperty(qmgrName +".name",qmgrName);
				config.addProperty(qmgrName +".hostname",server);
				config.addProperty(qmgrName +".port",port);
				config.addProperty(qmgrName +".channel",channel);
				config.addProperty(qmgrName +".user",username);
				config.addProperty(qmgrName +".password",password);
				config.addProperty(qmgrName +".isForAGI",isAGI);
				try {
					layout.save(new FileWriter(Variables.DirWork + "/qmgrs.properties", false));
			        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info: ", "New queue manager configuration added successfully!"));
			        NMTLogger.loguear("New queue manager added to qmgrs.properties: " + qmgrName + " " + server + " " + port);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "Property file not found"));
					NMTLogger.loguear(e.toString());
				} catch (IOException e) {
					// TODO Auto-generated catch block  
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "Something went wrong and the configuration could not be updated"));
					NMTLogger.loguear(e.toString());
				}
			}
			else {
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "There is another queue manager with the same name"));
			}
	        
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "Property file not found"));
		} catch (ConfigurationException e1) {
			// TODO Auto-generated catch block
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "Something went wrong and the configuration could not be updated"));
		}
		
	}
	
	public void removeQmgr() {
		
		try {
			File file = new File(Variables.DirWork + "/qmgrs.properties");
			PropertiesConfiguration config = new PropertiesConfiguration();
	        PropertiesConfigurationLayout layout = new PropertiesConfigurationLayout(config);
	        layout.load(new InputStreamReader(new FileInputStream(file)));
	        
			config.clearProperty(qmgrName+".name");
			config.clearProperty(qmgrName+".hostname");
			config.clearProperty(qmgrName+".port");
			config.clearProperty(qmgrName+".channel");
			config.clearProperty(qmgrName+".user");
			config.clearProperty(qmgrName+".password");
			config.clearProperty(qmgrName+".isForAGI");
			layout.save(new FileWriter(Variables.DirWork + "/qmgrs.properties", false));
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info: ", "Queue manager " + qmgrName + " removed successfuly!"));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error: ", "Something went wrong and the configuration could not be updated"));
			NMTLogger.loguear(e1.toString());
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			NMTLogger.loguear(e.toString());
		}
		
	}
}
